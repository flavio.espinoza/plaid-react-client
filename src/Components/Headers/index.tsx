import React, { useContext } from 'react';
import Callout from 'plaid-threads/Callout';
import Button from 'plaid-threads/Button';
import InlineLink from 'plaid-threads/InlineLink';

import Link from '../Link';
import Context from '../../Context';

import styles from './index.module.scss';

const Header = () => {
  const {
    itemId,
    accessToken,
    linkToken,
    linkSuccess,
    isItemAccess,
    backend,
    linkTokenError,
  } = useContext(Context);

  return (
    <div className={styles.grid}>
      <h3 className={styles.title}>Plaid Link Bank Account</h3>

      {!linkSuccess ? (
        <>
          <h4 className={styles.subtitle}>
            An end-to-end integration with Plaid
          </h4>
          <div className='mt-2'>
            <ul className='CheckList-module__container'>
              <li className='CheckList-module__checklistRow'>
                <div>
                  <h3 className='CheckList-module__primaryText'>Secure</h3>
                  <p className='CheckList-module__secondaryText'>
                    Encryption helps protect your personal financial data
                  </p>
                </div>
              </li>
              <li className='CheckList-module__checklistRow'>
                <div>
                  <h3 className='CheckList-module__primaryText'>Private</h3>
                  <p className='CheckList-module__secondaryText'>
                    Your credentials will never be made accessible to Plaid
                    Quickstart
                  </p>
                </div>
              </li>
            </ul>
          </div>
          {/* message if backend is not running and there is no link token */}
          {!backend ? (
            <Callout warning>
              Unable to fetch link_token: please make sure your backend server
              is running and that your .env file has been configured with your
              <code>PLAID_CLIENT_ID</code> and <code>PLAID_SECRET</code>.
            </Callout>
          ) : /* message if backend is running and there is no link token */
          linkToken == null && backend ? (
            <Callout warning>
              <div>
                Unable to fetch link_token: please make sure your backend server
                is running and that your .env file has been configured
                correctly.
              </div>
              <div>
                Error Code: <code>{linkTokenError.error_code}</code>
              </div>
              <div>
                Error Type: <code>{linkTokenError.error_type}</code>{' '}
              </div>
              <div>Error Message: {linkTokenError.error_message}</div>
            </Callout>
          ) : linkToken === '' ? (
            <div className={styles.linkButton}>
              <Button large disabled>
                Loading...
              </Button>
            </div>
          ) : (
            <div className={styles.linkButton}>
              <Link />
            </div>
          )}
        </>
      ) : (
        <>
          {isItemAccess ? (
            <h4 className={styles.subtitle}>
              Congrats! By linking an account, you have created an{' '}
              <InlineLink
                href='http://plaid.com/docs/quickstart/glossary/#item'
                target='_blank'
              >
                Item
              </InlineLink>
              .
            </h4>
          ) : (
            <h4 className={styles.subtitle}>
              <Callout warning>
                Unable to create an item. Please check your backend server
              </Callout>
            </h4>
          )}
          <div className={styles.itemAccessContainer}>
            <p className={styles.itemAccessRow}>
              <span className={styles.idName}>item_id</span>
              <span className={styles.tokenText}>{itemId}</span>
            </p>

            <p className={styles.itemAccessRow}>
              <span className={styles.idName}>access_token</span>
              <span className={styles.tokenText}>{accessToken}</span>
            </p>
          </div>
          {isItemAccess && (
            <p className={styles.requests}>
              Now that you have an access_token, you can make all of the
              following requests:
            </p>
          )}
        </>
      )}
    </div>
  );
};

Header.displayName = 'Header';

export default Header;
